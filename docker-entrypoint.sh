#!/bin/sh

cd /srv/superlists

echo "Running the superlists app!"
python3 manage.py migrate
python3 manage.py collectstatic --noinput

# Prepare log files and start outputting logs to stdout
touch /srv/superlists/logs/gunicorn.log
touch /srv/superlists/logs/access.log
tail -n 0 -f /srv/superlists/logs/*.log &

echo "Starting Gunicorn"
exec gunicorn superlists.wsgi:application \
     --name superlists \
     --bind 0.0.0.0:8000 \
     --workers 3 \
     --log-level=info \
     --log-file=/srv/superlists/logs/gunicorn.log \
     --access-logfile=/srv/superlists/logs/access.log \
     "$@"

